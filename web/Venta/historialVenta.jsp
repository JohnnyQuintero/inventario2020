<%-- 
    Document   : historialVenta
    Created on : 22/06/2020, 07:35:02 PM
    Author     : JHONY QUINTERO
--%>

<%@page import="modelo.Factura_Venta"%>
<%@page import="modelo.Factura_VentaDAO"%>
<%@page import="modelo.Detalle_Venta"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modelo.Detalle_VentaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table class="table">
  <thead>
    <tr>
      <th scope="col">ID_venta</th>
      <th scope="col">Cliente</th>
      <th scope="col">Id_Cliente</th>
      <th scope="col">Fecha</th>
      <th scope="col"> </th>
    </tr>
  </thead>
    <%
        Factura_VentaDAO f = new Factura_VentaDAO();
        for(Factura_Venta var : f.read()){%> 
        <tbody>
            <tr>
                <th scope="row"><%=var.getId_venta()%></th>
                <td><%=var.getNombre_cliente()%></td>
                <td><%=var.getId_cliente()%></td>
                <td><%=var.getFecha_venta().toString()%></td>
                <td><td><a href="../Venta/addDetalleVenta.jsp?id=<%=var.getId_venta()%>" button type="submit"  class="btn btn-outline btn-success">Agregar Productos</button></a></td>
            </tr>
        </tbody>
    <%}%>
</table>
 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>