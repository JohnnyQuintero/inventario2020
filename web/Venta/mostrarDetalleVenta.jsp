<%-- 
    Document   : mostrarDetalleVenta
    Created on : 23/06/2020, 09:13:14 AM
    Author     : JHONY QUINTERO
--%>

<%@page import="modelo.Producto"%>
<%@page import="modelo.ProductoDAO"%>
<%@page import="modelo.Detalle_Venta"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modelo.Detalle_VentaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

            <thead>
                <tr>
                    <th><b>CODIGO DEL PRODUCTO</b></th>
                    <th><b>NOMBRE</b></th>
                    <th><b>TIPO</b></th>
                    <th><b>VALOR C/U</b></th>
                    <th><b>CANTIDAD</b></th>
                    <th><b>VALOR TOTAL</b></th>
                    <th><b>OPERACIONES</b></th>
                </tr>
            </thead>
            <%
                Detalle_VentaDAO deta = new Detalle_VentaDAO();
                ProductoDAO pro = new ProductoDAO();
                ArrayList<Producto> pros = pro.read();
                ArrayList<Detalle_Venta> detas = new ArrayList();
                detas = deta.read();
                int idV = Integer.parseInt(request.getParameter("id"));
                for (Detalle_Venta aux : detas) {
                    if (aux.getId_venta() == idV) {
            %>
            <tr>
                <td><b><%=aux.getId_producto()%></b></td>
                <%
                    String nombre = "", tipo = "";
                    float precio = 0;
                    for (Producto p : pros) {
                        if (p.getId_producto() == aux.getId_producto()) {
                            nombre = p.getNombre_producto();
                            tipo = p.getTipo_producto();
                            precio = p.getPrecio_producto();
                            break;
                        }

                    }
                %>
                <td><b><%=nombre%></b></td>
                <td><b><%=tipo%></b></td>
                <td><b><%=aux.getPrecio_producto()%></b></td>
                <td><b>1</b></td>
                <td><b><%=precio%></b></td>
                <td><a href="../Venta/eliminarDetalle.jsp?id=<%=aux.getId_detalle()%>" button type="submit" class="btn btn-outline btn-danger">Eliminar</button></a></td>

            </tr>
            <%}
                }%>
        </table>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </body>
</html>
