<%-- 
    Document   : validarUsuario
    Created on : 20/06/2020, 10:25:58 AM
    Author     : JHONY QUINTERO
--%>
<%@page import="modelo.Usuario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modelo.UsuarioDAO"%>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<%
    String cc = request.getParameter("cedula");
    String clav = request.getParameter("clave");
    UsuarioDAO user = new UsuarioDAO();
    ArrayList<Usuario> users = new ArrayList();
    users = user.read();
    boolean correcto = false;
    for (Usuario x : users) {
        if (x.getContraseña().equals(clav)) {
            correcto = true;
        }
    }
%>
<% if (correcto) {%>
<jsp:forward page="../plantilla/principal.html"/>
<%} else {%>
<jsp:forward page="../plantilla/login.html"/>
<%}%>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
