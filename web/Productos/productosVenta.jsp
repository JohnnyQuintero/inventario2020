<%-- 
    Document   : mostrarproductos
    Created on : 30/05/2020, 01:03:32 PM
    Author     : JHONY QUINTERO
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="modelo.Producto"%>
<%@page import="modelo.ProductoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="../plantilla/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="../plantilla/css/sb-admin-2.min.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="card shadow mb-4">
            <div class="card-body">
                <table class="table table-bordered" class="text-primary" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><b class="text-body">CODIGO</b></th>
                            <th><b class="text-body">NOMBRE</b></th>
                            <th><b class="text-body">TIPO</b></th>
                            <th><b class="text-body">DESCRIPCION</b></th>
                            <th><b class="text-body">VALOR C/U</b></th>
                            <th> </th>
                        </tr>
                    </thead>
                    <%
                        ProductoDAO pro = new ProductoDAO();
                        ArrayList<Producto> pros = new ArrayList();
                        pros = pro.read();
                        int id = Integer.parseInt(request.getParameter("id"));
                        for(Producto aux:pros){
                        %>
                    <tr>
                        <td><b><%=aux.getId_producto()%></b></td>
                        <td><b><%=aux.getNombre_producto()%></b></td>
                        <td><b><%=aux.getTipo_producto()%></b></td>
                        <td><b><%=aux.getDescripcion_producto()%></b></td>
                        <td><b><%=aux.getPrecio_producto()%></b></td>
                        <td><a href="../Venta/añadirVentaDetalle.jsp?id=<%=aux.getId_producto()%>&idV=<%=id%>&precio=<%=aux.getPrecio_producto()%>" button type="submit" class="btn btn-outline btn-success">Agregar A La Venta</button></a></td>
                       </tr>
                    <%}%>
                </table>
            </div> 
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="../plantilla/vendor/jquery/jquery.min.js"></script>
        <script src="../plantilla/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="../plantilla/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="../plantilla/js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="../plantilla/vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="../plantilla/js/demo/chart-area-demo.js"></script>
        <script src="../plantilla/js/demo/chart-pie-demo.js"></script>
        <script src="../plantilla/js/demo/chart-bar-demo.js"></script>
    </body>
</html>
