<%-- 
    Document   : editarproducto
    Created on : 22/06/2020, 10:33:28 PM
    Author     : JHONY QUINTERO
--%>

<%@page import="modelo.Producto"%>
<%@page import="modelo.ProductoDAO"%>
<%
  ProductoDAO pro = new ProductoDAO();
  String nombre = request.getParameter("nombre");
  int id = Integer.parseInt(request.getParameter("id"));
  int cantidad = Integer.parseInt(request.getParameter("cant"));
  float valor = Float.parseFloat(request.getParameter("precio"));
  pro.update(new Producto(id,nombre,"","",cantidad,valor));
%>
<jsp:forward page="../plantilla/producto.jsp" />