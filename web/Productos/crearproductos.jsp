<%-- 
    Document   : crearproductos
    Created on : 22/06/2020, 04:03:05 PM
    Author     : JHONY QUINTERO
--%>

<%@page import="modelo.Producto"%>
<%@page import="modelo.ProductoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <meta charset="utf-8">

<%
    ProductoDAO pro = new ProductoDAO();
    pro.create(new Producto(0, request.getParameter("nombre"), request.getParameter("tipo"),
            request.getParameter("descripcion"), Integer.parseInt(request.getParameter("cant")),
            Float.parseFloat(request.getParameter("precio"))));
%>
<jsp:include page="../plantilla/producto.jsp"/>