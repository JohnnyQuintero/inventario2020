![inventario](https://lh3.googleusercontent.com/fwH4M4hJ8URTCywp-vLP7dP2EK_Zn9xn0JZDlWJZl2P2YmLiBu9PNoQtvFfRd8AQ6JwLfQ=s170)
# Nombre Del Proyecto
___
## Sistema de inventario para la empresa Distribuciones del Progreso
___
## Índice
1. [Características](#características)
😃
2. [Contenido del proyecto](#contenido-del-proyecto)
📋
3. [Tecnologías](#tecnologías)
🧠
4. [IDE](#ide)
📝
5. [Instalación](#instalación)
⬇
6. [Demo](#demo)
🚦
7. [Autor(es)](#autores)
📜
8. [Institución Académica](#institución-académica)
📚
9. [Referencias](#referencias)
📰
___

#### Características:
* Hace las funciones de registro, actualización y eliminación de la información de los productos que maneja la empresa.
* Automatización de los procesos con el manejo de la información internamente en la empresa.
* Diseño Responsivo.
* Interfaz Amigable para el usuario
___

#### Contenido del proyecto

| Archivo | Descipción |
| ------ | ------ |
| [src/java/Demo/DemoDb.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/Demo/DemoDb.java) | Hacer algunos test a la base de datos para verificar su funcionalidad |
| [src/java/bdatos/MysqlDb.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/bdatos/MysqlDb.java) | Establece y crea la conexión de la aplicación con la bases de datos  |
| [src/java/bdatos/PropertiesUtil.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/bdatos/PropertiesUtil.java) | Clase para poder leer el archivo de propiedades de la base de datos | 
| [src/java/modelo/Detalle_Compra.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Detalle_Compra.java) | Clase que contiene la instacia de las propiedades y respectivos metodos que cumple la función de detallar la compra de un producto |
| [src/java/modelo/Detalle_CompraDAO.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Detalle_CompraDAO.java) | Clase que permite hacer el CRUD en la base de datos de su correspondiente clase |
| [src/java/modelo/Detalle_Venta.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Detalle_Venta.java) | Clase que contiene la instacia de las propiedades y respectivos metodos que cumple la función de detallar la venta de un producto |
| [src/java/modelo/Detalle_VentaDAO.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Detalle_VentaDAO.java) | Clase que permite hacer el CRUD en la base de datos de su correspondiente clase |
| [src/java/modelo/Factura_Compra.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Factura_Compra.java) | Clase que contiene la instacia de las propiedades y respectivos metodos que cumple la función de facturar la compra de un producto |
| [src/java/modelo/Factura_CompraDAO.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Factura_CompraDAO.java) | Clase que permite hacer el CRUD en la base de datos de su correspondiente clase |
| [src/java/modelo/Factura_Venta.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Factura_Venta.java) | Clase que contiene la instacia de las propiedades y respectivos metodos que cumple la función de facturar la venta de un producto |
| [src/java/modelo/Factura_Venta.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Factura_Venta.java) | Clase que permite hacer el CRUD en la base de datos de su correspondiente clase |
| [src/java/modelo/Producto.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Producto.java) | Clase que contiene la instacia de las propiedades y respectivos metodos que cumple la función de gestionar un producto | 
| [src/java/modelo/ProductoDAO.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/ProductoDAO.java) | Clase que permite hacer el CRUD en la base de datos de su correspondiente clase |
| [src/java/modelo/Usuario.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Usuario.java) | Clase que contiene la instacia de las propiedades y respectivos metodos que cumple la función de gestionar un usario | 
| [src/java/modelo/UsuarioDAO.java](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/modelo/Usuario.java) | Clase que permite hacer el CRUD en la base de datos de su correspondiente clase |
| [src/java/propiedades/propiedades.properties](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/src/java/propiedades/propiedades.properties) | Contiene la información de las propiedades de la base de datos para su conexión |
| [web/Login/crearusuario.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Login/crearusuario.jsp) | Cumple con la funcionalidad de crear un nuevo usuario en la base de datos |
| [web/Login/validarUsuario.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Login/validarUsuario.jsp) | Cumple con la funcionaldad de verificar el inicio de sesión a un usuario |
| [web/Productos/crearproductos.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Productos/crearproductos.jsp) | Permite el registro de productos nuevos en la base de datos |
| [web/Productos/editarproducto.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Productos/editarproducto.jsp) | Permite la actualización de la información sobre un producto en la base de datos|
| [web/Productos/eliminarproducto.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Productos/eliminarproducto.jsp) | Permite eliminar los productos seleccionados en la base de datos |
| [web/Productos/mostrarproductos.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Productos/mostrarproductos.jsp) | Permite traer toda la información sobre los produtos de la base de datos |
| [web/Productos/productosVenta.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Productos/productosVenta.jsp) | Permite registrar los productos que estan asociados a una venta en la base de datos |
| [web/Venta/addDetalleVenta.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Venta/addDetalleVenta.jsp) | Permite registrar la venta con todo los precios y productos en la base de datos |
| [web/Venta/addFactura.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Venta/addFactura.jsp) | Permite Crear y registrar una factura que va asociada a cada cliente que a su vez esta realizando una compra |
| [web/Venta/eliminarDetalle.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Venta/eliminarDetalle.jsp) | Permite eliminar cada producto que esta asociado a una venta, cabe aclara que la venta todavia no ha sido facturada |
| [web/Venta/mostrarDetalleVenta.jsp](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/Venta/mostrarDetalleVenta.jsp) | Permite visualizar la información de cada producto que se esta registrando para una venta |
| [web/plantilla](https://gitlab.com/JohnnyQuintero/inventario2020/-/tree/master/web/plantilla) | En esta carpeta en general contiene toda la parte grafica del software, sin embargo, cabe aclarar que este proyecto fue realizado por una plantilla gratis descargada desde internet, para mas información de click [aqui](https://startbootstrap.com/). |
| [web/img/login.jpg](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/img/login.jpg) | imagen que se encuentra situada en el login |
| [web/plantilla/css](https://gitlab.com/JohnnyQuintero/inventario2020/-/tree/master/web/plantilla/css) | Carpeta que contiene todo el css que se trabajo en el proyecto |
| [web/plantilla/js](https://gitlab.com/JohnnyQuintero/inventario2020/-/tree/master/web/plantilla/js) | Carpeta que se encuentra cada archivo de JavaScript, con sus respectivas funciones que permiten dar dinamismo e interación a la pagina |
| [web/plantilla/login.html](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/plantilla/login.html) | Se encarga de visualizar y tambien permite modificar el archivo sobre la estructura de la interfaz del login |
| [web/plantilla/agregarproducto.html](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/plantilla/agregarproducto.html) | Se encarga de visualizar y tambien permite modificar el archivo sobre la estructura de la interfaz de agregar productos a una venta |
| [web/plantilla/historialInventario.html](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/plantilla/historialInventario.html) | Se encarga de visualizar y tambien permite modificar el archivo sobre la estructura de la interfaz de historial del inventario |
| [web/plantilla/principal.html](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/plantilla/principal.html) | Se encarga de visualizar y tambien permite modificar el archivo sobre la estructura de la interfaz principal de la app |
| [web/plantilla/register.html](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/plantilla/register.html) | Se encarga de visualizar y tambien permite modificar el archivo sobre la estructura de la interfaz del registro de usuarios | 
| [web/plantilla/registrarCompra.html](https://gitlab.com/JohnnyQuintero/inventario2020/-/blob/master/web/plantilla/registrarCompra.html) | Se encarga de visualizar y tambien permite modificar el archivo sobre la estructura de la interfaz del registro de productos en la compra|
___
#### Tecnologías
___
* **Para el despliegue de la pagina web se uso:**

1. [![Nube de AWS](https://img.shields.io/badge/AWS-yellow)](https://aws.amazon.com/es/)  Amazon Web Services engloba una gran cantidad de servicios para poder realizar distintos tipos de actividades en la nube. Desde almacenamiento a la gestión de instancias, imágenes virtuales, desarrollo de aplicaciones móviles. [(Portal, 2021)](#ref-3-portal-t-2021-2-febrero-amazon-web-services-aws-qué-es-y-qué-ofrece-tic-portal-httpswwwticportalestemascloud-computingamazon-web-services)
___
* **Backend:**
1. [![JAVA](https://img.shields.io/badge/JAVA-red)](https://www.java.com/es/)  Java es un lenguaje de propósito general capaz de acometer todo tipo de proyectos y ejecutarse en múltiples plataformas. Aquí aprenderás qué es Java y a programar en este lenguaje con diversos manuales. [(Java, 2016)](#ref-4-java-2016-desarrollo-web-httpsdesarrollowebcomhomejava)
___
* **Frontend:**
1. [![HTML5](https://img.shields.io/badge/HTML5-CSS-green)](https://developer.mozilla.org/es/docs/Web/Guide/HTML/HTML5)  HTML5 es la última versión de HTML. El término representa dos conceptos diferentes: Se trata de una nueva versión de HTML, con nuevos elementos, atributos y comportamientos. Contiene un conjunto más amplio de tecnologías que permite a los sitios Web y a las aplicaciones ser más diversas y de gran alcance. [(HTML5 - Guía de Desarrollo Web | MDN, 2021)](#ref-5-html5-guía-de-desarrollo-web-mdn-2021-27-abril-developer-mozilla-httpsdevelopermozillaorgesdocswebguidehtmlhtml5)
___
2. [![BOOTSTRAP](https://img.shields.io/badge/BOOTSTRAP-blueviolet)](https://getbootstrap.com)  Bootstrap, es un framework originalmente creado por Twitter, que permite crear interfaces web con CSS y JavaScript, cuya particularidad es la de adaptar la interfaz del sitio web al tamaño del dispositivo en que se visualice. [(2014)](#ref-6-a-2014-27-septiembre-qué-es-bootstrap-y-cómo-funciona-en-el-diseño-web-blog-arweb-agencia-digital-costa-rica-httpswwwarwebcomblogc2bfque-es-bootstrap-y-como-funciona-en-el-diseno-web7etextbootstrap2c20es20un20framework20originalmentedispositivo20en20que20se20visualice)
___
3. [![JAVASCRIPT](https://img.shields.io/badge/JAVASCRIPT-green)](https://developer.mozilla.org/es/docs/Web/JavaScript)  JavaScript (JS) es un lenguaje de programación ligero, interpretado, o compilado justo-a-tiempo (just-in-time) con funciones de primera clase. Si bien es más conocido como un lenguaje de scripting (secuencias de comandos) para páginas web, y es usado en muchos entornos fuera del navegador, tal como Node.js, Apache CouchDB y Adobe Acrobat. [(JavaScript | MDN, 2021)](#ref-7-javascript-mdn-2021-27-abril-developer-mozilla-httpsdevelopermozillaorgesdocswebjavascript)
___
* **Bases de datos:**
1. [![MYSQL](https://img.shields.io/badge/MYSQL-blue)](https://www.mysql.com)  MySQL es un sistema de gestión de bases de datos relacional desarrollado bajo licencia dual: Licencia pública general/Licencia comercial por Oracle Corporation y está considerada como la base de datos de código abierto más popular del mundo,​​ y una de las más populares en general junto a Oracle y Microsoft SQL Server, todo para entornos de desarrollo web. [(colaboradores de Wikipedia, 2021)](#ref-8-colaboradores-de-wikipedia-2021b-abril-24mysql-wikipedia-la-enciclopedia-libre-httpseswikipediaorgwikimysql7etextmysql20es20un20sistema20deoracle20y20microsoft20sql20server2c)
___
* **Otros:**
1. [![JSP](https://img.shields.io/badge/JSP-inactive)](https://www.it.uc3m.es/labttlat/2007-08/material/jsp/)  JSP es un acrónimo de Java Server Pages.  Es, pues, una tecnología orientada a crear páginas web con programación en Java. Con JSP podemos crear aplicaciones web que se ejecuten en variados servidores web, ya que Java es en esencia un lenguaje multiplataforma. Las páginas JSP están compuestas de código HTML/XML mezclado con etiquetas especiales para programar scripts de servidor en sintaxis. [Java.(Qué Es JSP, 2002)](#ref-9-qué-es-jsp-2002-8-julio-desarrollo-web-httpsdesarrollowebcomarticulos831php)
___
#### IDE

* El Proyecto desarrollado se compono de dos partes, el Backend y Fronted por ende se usaron dos IDE para el desarrollo.

| IDE | icono |
| ------ | ------ |
| Para el Backend se uso el IDE NETBEANS version [8.2](https://www.oracle.com/technetwork/java/javase/downloads/jdk-netbeans-jsp-3413139-esa.html), este provee Soporte para la creación de aplicaciones orientadas a servicios (SOA), incluyendo herramientas de esquemas XML, un editor WSDL, y un editor BPEL para web services (colaboradores de Wikipedia, 2021). | ![netbeans-img](https://image.slidesharecdn.com/presentacionnetbeans-150918190424-lva1-app6892/95/presentacin-de-netbeans-2-638.jpg?cb=1442603130) |
|  Para el Fronted se uso el IDE Visual Studio Code 2019 ([VScode](https://code.visualstudio.com/?wt.mc_id=DX_841432)), es una aplicación informática cuya finalidad es simplemente hacer la vida de los desarrolladores y programadores mucho más fácil, al ofrecer servicios integrales que facilitan desarrollar el software de manera mucho más sencilla. (Innova Advanced Consulting, 2019) | ![VScode-img](https://res.cloudinary.com/practicaldev/image/fetch/s--LiYXrus5--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://code4coders.files.wordpress.com/2019/05/008ce-1cn_xbd307e3lobhk511qqg.png%3Fw%3D700%26zoom%3D2%2522%2520Logo%2520Title%2520Text%25201%2522) |

___
#### instalación

1. De manera local:

Para ello debe dirigirse a el siguiente [enlace](https://gitlab.com/JohnnyQuintero/inventario2020) y descargar de manera local en proyecto, posteriormente debe abrir la carpeta "web" y a su vez dentro de esa carpeta abrir la carpeta "plantilla" para ubicar el archivo principal.html y ejecutarlo.
___
### Demo
Para ver el demo del software dirigirse a: [Distribuciones del Progreso](https://johnnyquintero.gitlab.io/inventario2020/web/plantilla/principal.html)
___
### Autor(es)
Proyecto desarrollado por:
* [**Johnny Alexander Quintero Reyes**](https://johnnyquintero.gitlab.io/curriculum/index.html) (<johnnyalexanderqr@ufps.edu.co>).
* carlos mauricio pabon marulanda (<carlosmauriciopama@ufps.edu.co>)

___
### Institución Académica   
Proyecto desarrollado en la Materia programación Orientada a Objetos - II del [Programa de Ingeniería de Sistemas](https://ingsistemas.cloud.ufps.edu.co/) de la [Universidad Francisco de Paula Santander](https://ww2.ufps.edu.co/)
___
### Referencias
##### Ref 1: colaboradores de Wikipedia. (2021, 13 marzo). NetBeans. Wikipedia, la enciclopedia libre. https://es.wikipedia.org/wiki/NetBeans
##### Ref 2: Innova Advanced Consulting. (2019, 7 junio). ¿Qué es Visual Studio Code (VS code) y por qué lo necesitas para Dynamics 365? Innova. https://www.innovaconsulting.es/blog/visual-studio-code-vs-code/:%7E:text=Visual%20Studio%20Code%20se%20presenta%20como%20un%20excelente%20aliado%20empresarial.&text=Visual%20Studio%20es%20una%20aplicaci%C3%B3n,de%20manera%20mucho%20m%C3%A1s%20sencilla.
##### Ref 3: Portal, T. (2021, 2 febrero). Amazon Web Services (AWS) : ¿qué es y qué ofrece? TIC Portal. https://www.ticportal.es/temas/cloud-computing/amazon-web-services
##### Ref 4: Java. (2016). Desarrollo Web. https://desarrolloweb.com/home/java
##### Ref 5: HTML5 - Guía de Desarrollo Web | MDN. (2021, 27 abril). Developer Mozilla. https://developer.mozilla.org/es/docs/Web/Guide/HTML/HTML5
##### Ref 6: A. (2014, 27 septiembre). ¿Qué es Bootstrap y cómo funciona en el diseño web? Blog, ARWEB Agencia Digital Costa Rica. https://www.arweb.com/blog/%C2%BFque-es-bootstrap-y-como-funciona-en-el-diseno-web/:%7E:text=Bootstrap%2C%20es%20un%20framework%20originalmente,dispositivo%20en%20que%20se%20visualice.
##### Ref 7: JavaScript | MDN. (2021, 27 abril). Developer Mozilla. https://developer.mozilla.org/es/docs/Web/JavaScript 
##### Ref 8: colaboradores de Wikipedia. (2021b, abril 24).MySQL. Wikipedia, la enciclopedia libre. https://es.wikipedia.org/wiki/MySQL:%7E:text=MySQL%20es%20un%20sistema%20de,Oracle%20y%20Microsoft%20SQL%20Server%2C
##### Ref 9: Qué es JSP. (2002, 8 julio). Desarrollo Web. https://desarrolloweb.com/articulos/831.php
