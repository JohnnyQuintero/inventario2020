/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author JHONY QUINTERO
 */
public class Detalle_Compra {

    private int id_detalle;
    private int id_producto;
    private int id_venta;
    private int cant_producto;
    private float precio_producto;
    private float precio_total;

    public Detalle_Compra() {
    }

    public Detalle_Compra(int id_detalle, int id_producto, int id_venta, int cant_producto, float precio_producto, float precio_total) {
        this.id_detalle = id_detalle;
        this.id_producto = id_producto;
        this.id_venta = id_venta;
        this.cant_producto = cant_producto;
        this.precio_producto = precio_producto;
        this.precio_total = precio_total;
    }

    public int getId_detalle() {
        return id_detalle;
    }

    public void setId_detalle(int id_detalle) {
        this.id_detalle = id_detalle;
    }

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public int getId_venta() {
        return id_venta;
    }

    public void setId_venta(int id_venta) {
        this.id_venta = id_venta;
    }

    public int getCant_producto() {
        return cant_producto;
    }

    public void setCant_producto(int cant_producto) {
        this.cant_producto = cant_producto;
    }

    public float getPrecio_producto() {
        return precio_producto;
    }

    public void setPrecio_producto(float precio_producto) {
        this.precio_producto = precio_producto;
    }

    public float getPrecio_total() {
        return precio_total;
    }

    public void setPrecio_total(float precio_total) {
        this.precio_total = precio_total;
    }

    @Override
    public String toString() {
        return "Detalle_Compra{" + "id_detalle=" + id_detalle + ", id_producto=" + id_producto + ", id_venta=" + id_venta + ", cant_producto=" + cant_producto + ", precio_producto=" + precio_producto + ", precio_total=" + precio_total + '}';
    }
    

}
