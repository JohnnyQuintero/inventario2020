/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JHONY QUINTERO
 */
public class Detalle_VentaDAO {

    public void create(Detalle_Venta detalleVenta) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO detalle_venta VALUES (NULL,?,?,?,?,?)");
            statement.setInt(1, detalleVenta.getId_producto());
            statement.setInt(2, detalleVenta.getId_venta());
            statement.setInt(3, detalleVenta.getCant_producto());
            statement.setFloat(4, detalleVenta.getPrecio_producto());
            statement.setFloat(5, detalleVenta.getPrecio_total());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Detalle_Venta> read() {
        ArrayList<Detalle_Venta> ventas = new ArrayList();
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            String sql = "SELECT * FROM detalle_venta";
            Statement setencia = connection.prepareStatement(sql);
            ResultSet resultado = setencia.executeQuery(sql);
            while (resultado.next()) {
                ventas.add(new Detalle_Venta(resultado.getInt("id_detalle"),
                        resultado.getInt("id_producto"),
                        resultado.getInt("id_venta"),
                        resultado.getInt("cant_producto"),
                        resultado.getFloat("precio_producto"),
                        resultado.getFloat("precio_total")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ventas;
    }

    public void update(Detalle_Venta detalleVenta) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE detalle_venta SET cant_producto = ?, precio_producto = ? WHERE id_detalle = ?");
            statement.setInt(1, detalleVenta.getCant_producto());
            statement.setFloat(2, detalleVenta.getPrecio_producto());
            statement.setInt(3, detalleVenta.getId_detalle());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM detalle_venta WHERE id_detalle=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
