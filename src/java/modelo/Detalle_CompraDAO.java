/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JHONY QUINTERO
 */
public class Detalle_CompraDAO {

    public void create(Detalle_Compra detalleCompra) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO detalle_compra VALUES (NULL,?,?,?,?)");
            statement.setInt(1, detalleCompra.getId_producto());
            statement.setInt(1, detalleCompra.getId_venta());
            statement.setInt(2, detalleCompra.getCant_producto());
            statement.setFloat(3, detalleCompra.getPrecio_producto());
            statement.setFloat(4, detalleCompra.getPrecio_total());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Detalle_Compra> read() {
        ArrayList<Detalle_Compra> compras = new ArrayList();
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            String sql = "SELECT * FROM detalle_compra";
            Statement setencia = connection.prepareStatement(sql);
            ResultSet resultado = setencia.executeQuery(sql);
            while (resultado.next()) {
                compras.add(new Detalle_Compra(resultado.getInt("id_detalle"),
                        resultado.getInt("id_producto"),
                        resultado.getInt("id_venta"),
                        resultado.getInt("cantidad_producto"),
                        resultado.getFloat("precio_producto"),
                        resultado.getFloat("precio_total")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return compras;
    }

    public void update(Detalle_Compra detalleCompra) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE detalle_compra SET cant_producto = ?, precio_producto = ? WHERE id_detalle = ?");
            statement.setInt(1, detalleCompra.getCant_producto());
            statement.setFloat(2, detalleCompra.getPrecio_producto());
            statement.setInt(3, detalleCompra.getId_detalle());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM detalle_compra WHERE id_detalle=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
