/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Date;

/**
 *
 * @author JHONY QUINTERO
 */
public class Factura_Venta {

    private int id_venta;
    private String fecha_venta;
    private String nombre_cliente;
    private int id_cliente;

    public Factura_Venta() {
    }

    public Factura_Venta(int id_venta, String fecha_venta, String nombre_cliente, int id_cliente) {
        this.id_venta = id_venta;
        this.fecha_venta = fecha_venta;
        this.nombre_cliente = nombre_cliente;
        this.id_cliente = id_cliente;
    }

    public int getId_venta() {
        return id_venta;
    }

    public void setId_venta(int id_venta) {
        this.id_venta = id_venta;
    }

    public String getFecha_venta() {
        return fecha_venta;
    }

    public void setFecha_venta(String fecha_venta) {
        this.fecha_venta = fecha_venta;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

}
