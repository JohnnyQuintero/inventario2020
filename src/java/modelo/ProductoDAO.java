/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JHONY QUINTERO
 */
public class ProductoDAO {

    public void create(Producto producto) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO producto VALUES (NULL,?,?,?,?,?)");
            
            statement.setString(1, producto.getNombre_producto());
            statement.setString(2, producto.getTipo_producto());
            statement.setString(3, producto.getDescripcion_producto());
            statement.setInt(4, producto.getCant_producto());
            statement.setFloat(5, producto.getPrecio_producto());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Producto> read() {
        ArrayList<Producto> productos = new ArrayList();
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            String sql = "SELECT * FROM producto";
            Statement setencia = connection.prepareStatement(sql);
            ResultSet resultado = setencia.executeQuery(sql);
            while (resultado.next()) {
                productos.add(new Producto(resultado.getInt("id_producto"),
                        resultado.getString("nombre_producto"),
                        resultado.getString("tipo_producto"),
                        resultado.getString("descripcion_producto"),
                        resultado.getInt("cant_producto"),
                        resultado.getFloat("precio_producto")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return productos;
    }

    public void update(Producto producto) {

        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE producto SET nombre_producto = ?, cant_producto = ?, precio_producto = ? WHERE id_producto = ?");
            statement.setString(1, producto.getNombre_producto());
            statement.setInt(2, producto.getCant_producto());
            statement.setFloat(3, producto.getPrecio_producto());
            statement.setInt(4, producto.getId_producto());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM producto WHERE id_producto=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
