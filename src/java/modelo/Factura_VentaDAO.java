/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JHONY QUINTERO
 */
public class Factura_VentaDAO {

    public void create(Factura_Venta factura) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO factura_venta VALUES (NULL,?,?,?)");
            statement.setString(1, factura.getFecha_venta());
            statement.setString(2, factura.getNombre_cliente());
            statement.setInt(3, factura.getId_cliente());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Factura_Venta> read() {
        ArrayList<Factura_Venta> facs = new ArrayList();
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            String sql = "SELECT * FROM factura_venta";
            Statement setencia = connection.prepareStatement(sql);
            ResultSet resultado = setencia.executeQuery(sql);
            while (resultado.next()) {
                facs.add(new Factura_Venta(resultado.getInt("id_venta"),
                        resultado.getString("fecha_venta"),
                        resultado.getString("nombre_cliente"),
                        resultado.getInt("id_cliente")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return facs;
    }

    public void update(Factura_Venta facturaVenta) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE factura_venta SET fecha_venta = ?, nombre_cliente = ? WHERE id_venta = ?");
            statement.setString(1, facturaVenta.getFecha_venta());
            statement.setString(2, facturaVenta.getNombre_cliente());
            statement.setInt(3, facturaVenta.getId_cliente());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM factura_venta WHERE id_venta=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
