/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Date;

/**
 *
 * @author JHONY QUINTERO
 */
public class Factura_Compra {

    private int id_compra;
    private Date fecha_Compra;
    private String nombre_proveedor;
    private int id_proovedor;

    public Factura_Compra() {
    }

    public Factura_Compra(int id_compra, Date fecha_Compra, String nombre_proveedor, int id_proovedor) {
        this.id_compra = id_compra;
        this.fecha_Compra = fecha_Compra;
        this.nombre_proveedor = nombre_proveedor;
        this.id_proovedor = id_proovedor;
    }

    public int getId_compra() {
        return id_compra;
    }

    public void setId_compra(int id_compra) {
        this.id_compra = id_compra;
    }

    public Date getFecha_Compra() {
        return fecha_Compra;
    }

    public void setFecha_Compra(Date fecha_Compra) {
        this.fecha_Compra = fecha_Compra;
    }

    public String getNombre_proveedor() {
        return nombre_proveedor;
    }

    public void setNombre_proveedor(String nombre_proveedor) {
        this.nombre_proveedor = nombre_proveedor;
    }

    public int getId_proovedor() {
        return id_proovedor;
    }

    public void setId_proovedor(int id_proovedor) {
        this.id_proovedor = id_proovedor;
    }

    @Override
    public String toString() {
        return "Factura_Compra{" + "id_compra=" + id_compra + ", fecha_Compra=" + fecha_Compra + ", nombre_cliente=" + nombre_proveedor + ", id_proovedor=" + id_proovedor + '}';
    }

}
