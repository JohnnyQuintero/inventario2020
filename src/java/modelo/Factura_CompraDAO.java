/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JHONY QUINTERO
 */
public class Factura_CompraDAO {

    public void create(Factura_Compra factura) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO factura_compra VALUES (NULL,?,?,?)");
            statement.setDate(1, factura.getFecha_Compra());
            statement.setString(2, factura.getNombre_proveedor());
            statement.setInt(3, factura.getId_proovedor());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Factura_Compra> read() {
        ArrayList<Factura_Compra> facs = new ArrayList();
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            String sql = "SELECT * FROM factura_compra";
            Statement setencia = connection.prepareStatement(sql);
            ResultSet resultado = setencia.executeQuery(sql);
            while (resultado.next()) {
                facs.add(new Factura_Compra(resultado.getInt("id_compra"),
                        resultado.getDate("fecha_compra"),
                        resultado.getString("nombre_cliente"),
                        resultado.getInt("id_proovedor")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return facs;
    }

    public void update(Factura_Compra facturaCompra) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE factura_compra SET fecha_compra = ?, nombre_proveedor = ? WHERE id_compra = ?");
            statement.setDate(1, facturaCompra.getFecha_Compra());
            statement.setString(2, facturaCompra.getNombre_proveedor());
            statement.setInt(3, facturaCompra.getId_compra());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM factura_compra WHERE id_compra=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
