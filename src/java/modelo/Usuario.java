/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Objects;

/**
 *
 * @author JHONY QUINTERO
 */
public class Usuario {
    private int cc;
    private String nombre;
    private String contraseña;

    public Usuario() {
    }

    public Usuario(int cc, String nombre, String contraseña) {
        this.cc = cc;
        this.nombre = nombre;
        this.contraseña = contraseña;
    }

    public int getCc() {
        return cc;
    }

    public void setCc(int cc) {
        this.cc = cc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.contraseña);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (!Objects.equals(this.contraseña, other.contraseña)) {
            return false;
        }
        return true;
    }

    
    @Override
    public String toString() {
        return "Usuario{" + "cc=" + cc + ", nombre=" + nombre + ", contrase\u00f1a=" + contraseña + '}';
    }
    
    
}
