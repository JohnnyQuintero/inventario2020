/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import bdatos.MysqlDb;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author JHONY QUINTERO
 */
public class UsuarioDAO {

    public void create(Usuario user) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO usuario VALUES (?,?,?)");
            statement.setInt(1, user.getCc());
            statement.setString(2, user.getNombre());
            statement.setString(3, user.getContraseña());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Usuario> read() {
        ArrayList<Usuario> usuarios = new ArrayList();
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            String sql = "SELECT * FROM usuario";
            Statement setencia = connection.prepareStatement(sql);
            ResultSet resultado = setencia.executeQuery(sql);
            while (resultado.next()) {
                usuarios.add(new Usuario(resultado.getInt("cedula"),
                        resultado.getString("nombre"),
                        resultado.getString("contraseña")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuarios;
    }

    public void update(Usuario user) {

        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE usuario SET nombre = ?, contraseña=? WHERE cedula = ?");
            statement.setString(1, user.getNombre());
            statement.setString(2, user.getContraseña());
            statement.setInt(3, user.getCc());
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(int id) {
        MysqlDb mysqlDb = new MysqlDb();
        Connection connection = mysqlDb.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("DELETE FROM usuario WHERE cedula=?");
            statement.setInt(1, id);
            statement.executeUpdate();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
