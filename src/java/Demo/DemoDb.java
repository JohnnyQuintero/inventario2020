/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Demo;

import bdatos.MysqlDb;
import java.sql.Date;
import java.util.ArrayList;
import modelo.Detalle_Compra;
import modelo.Detalle_CompraDAO;
import modelo.Detalle_Venta;
import modelo.Detalle_VentaDAO;
import modelo.Factura_Compra;
import modelo.Factura_CompraDAO;
import modelo.Factura_Venta;
import modelo.Factura_VentaDAO;
import modelo.Producto;
import modelo.ProductoDAO;
import modelo.Usuario;
import modelo.UsuarioDAO;

/**
 *
 * @author Matias
 */
public class DemoDb {

    public static void main(String[] args) {
        MysqlDb m = new MysqlDb();
        m.getConnection();
    }

    public static void testDetalleCompra() {
        Detalle_CompraDAO detalle = new Detalle_CompraDAO();
        detalle.create(new Detalle_Compra(1, 1, 1, 10, 1200, 1000));
        ArrayList<Detalle_Compra> detalles = new ArrayList();
        detalles = detalle.read();
        for (Detalle_Compra aux : detalles) {
            System.out.println(aux.toString());
        }
    }

    public static void testDetalleVenta() {
        Detalle_VentaDAO detalle = new Detalle_VentaDAO();
        detalle.create(new Detalle_Venta(1, 1, 1, 10, 1200, 1000));
        ArrayList<Detalle_Venta> detalles = new ArrayList();
        detalles = detalle.read();
        for (Detalle_Venta aux : detalles) {
            System.out.println(aux.toString());
        }
    }

    public static void testFacturaCompra() {
        Factura_CompraDAO fac = new Factura_CompraDAO();
        fac.create(new Factura_Compra(1, new Date(1, 12, 2001), "Johnny", 16));
        ArrayList<Factura_Compra> facs = new ArrayList();
        facs = fac.read();
        for (Factura_Compra aux : facs) {
            System.out.println(aux.toString());

        }
    }

    public static void testFacturaVenta() {
        Factura_VentaDAO fac = new Factura_VentaDAO();
        ArrayList<Factura_Venta> facs = new ArrayList();
        facs = fac.read();
        for (Factura_Venta aux : facs) {
            System.out.println(aux.toString());

        }
    }

    public static void testProdcuto() {
        ProductoDAO pro = new ProductoDAO();
        //pro.create(new Producto(1, "Ariel","Jabon", "Sirve para lavar", 10, 1000));
        ArrayList<Producto> pros = new ArrayList();
        pros = pro.read();
        for (Producto aux : pros) {
            System.out.println(aux.toString());
        }
        pro.update(new Producto(1,"ARROZ saborsito RICOLINO","jabon","sirve pa esp",100000000,1000));
        
    }
    
    public static void testUsuario(){
        UsuarioDAO user = new UsuarioDAO();
        //user.create(new Usuario(1111,"Johnny","123412"));
        ArrayList<Usuario> users = new ArrayList();
        users = user.read();
        for(Usuario aux:users){
            System.out.println(aux.toString());
        }
    }

}
